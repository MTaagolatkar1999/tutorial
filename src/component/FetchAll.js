import axios from "axios";
import { useState } from "react";

export default function FetchAll() {
  const [response, setResponse] = useState([]);
  const ApiCall = () => {
    axios
      .get(`http://localhost:3001/api/fetchAll`)
      .then(function (response) {
        console.log(response.data);
        setResponse(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <div>
      <button onClick={ApiCall}>View All</button>
      <table
        border="2px"
        cellPadding="5px"
        cellSpacing="5px"
        rules="All"
        className="table table-bordered"
      >
        <tr>
          <th>Id</th>
          <th>Movie Name</th>
          <th>Movie Review</th>
        </tr>
        {response.map((item) => {
          return (
            <tr key={item.id}>
              <td>{item.id}</td>
              <td>{item.movie_name}</td>
              <td>{item.movie_review}</td>
            </tr>
          );
        })}
      </table>
    </div>
  );
}
