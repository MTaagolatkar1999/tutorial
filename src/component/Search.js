import { useState } from "react";

export default function Search({ handleSearch }) {
  const [query, setQuery] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log("Submit is called");
    handleSearch(query);
  };

  return (
    <div>
      <div className="ui divider"></div>
      <form className="ui fluid form" onSubmit={handleSubmit}>
        <div className="field">
          <input
            type="text"
            placeholder="Search"
            onChange={(e) => {
              setQuery(e.target.value);
            }}
          />
        </div>
      </form>
    </div>
  );
}
