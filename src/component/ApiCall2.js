import axios from "axios";
import { useState } from "react";
import Search from "./Search";
export default function ApiCall2() {
  const [data, setData] = useState([]);
  const [username, setUsername] = useState("");
  const api1 = () => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then(function (response) {
        console.log(response.data);
        setData(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const handleSearch = (search) => {
    console.log("Handle Search", search);
    setUsername(search);
  };

  return (
    <div>
      <button onClick={api1} className="ui red basic button">
        Call api
      </button>
      <Search handleSearch={handleSearch} />
      <div>
        {data
          .filter((item, index) => {
            if (username == "") {
              return data;
            } else if (
              item.name.toLowerCase().includes(username.toLowerCase())
            ) {
              return item;
            }
          })
          .map((item, index) => {
            return (
              <div className="ui card" key={index}>
                <div className="content">
                  <div className="center aligned header">{item.name}</div>
                  <p className="center aligned">{item.email}</p>
                  <div className="center aligned description">
                    <p>
                      {item.address.street}, {item.address.suite}
                    </p>
                    <p>
                      {item.address.city}, {item.address.zipcode}
                    </p>
                  </div>
                </div>
                <div className="extra content">
                  <div className="center aligned author">{item.username}</div>
                </div>
              </div>
            );
          })}
      </div>
    </div>
  );
}
