// kO9W0Z1G6p36BF0o9Zw479sBJ6FByv5I4YyfJ50qwyc
import Search from "./Search";
import { useState } from "react";
import axios from "axios";
import { saveAs } from "file-saver";
export default function Unsplash() {
  const [search, setSearch] = useState("");
  const [result, setResult] = useState([]);

  const instance = axios.create({
    baseURL: "https://api.unsplash.com/",
    headers: {
      Authorization: "Client-ID kO9W0Z1G6p36BF0o9Zw479sBJ6FByv5I4YyfJ50qwyc",
    },
  });
  const ApiCall = () => {
    instance
      .get("/search/photos", {
        params: {
          query: search,
        },
      })
      .then(function (response) {
        console.log(response.data);
        if (response.data.total != 0) {
          setResult(response.data.results);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const handleSearch = (search) => {
    console.log("Handle Search", search);
    setSearch(search);
    ApiCall();
  };

  return (
    <div>
      <h1>Unsplash Search</h1>
      <Search handleSearch={handleSearch} />

      <div>
        {result &&
          result.map((item, index) => {
            return (
              <div className="ui small image" key={item.links.id}>
                <img
                  src={item.urls.raw}
                  alt={item.alt_description}
                  height="100px"
                  onClick={(event) => {
                    saveAs(event.target.src, event.target.alt + ".jpg");
                  }}
                  id={"image" + index}
                />
              </div>
            );
          })}
      </div>
    </div>
  );
}
