import axios from "axios";
export default function ApiCall() {
  const Api1 = () => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });

    console.log("fun");
  };

  const Api2 = () => {
    axios
      .get("https://jsonplaceholder.typicode.com/comments", {
        params: {
          postId: 2,
        },
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  // async await async Api3(){await} 2017
  const Api3 = async () => {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  const Api4 = () => {
    axios
      .get("https://jsonplaceholder.typicode.com/comments", {
        params: {
          postId: 1,
        },
      })
      .then(function (response) {
        console.log("Post ", response);
      })
      .catch(function (error) {
        console.log("Error", error);
      });
  };

  const instance = axios.create({
    baseURL: "https://jsonplaceholder.typicode.com/comments",
  });

  const Api5 = () => {
    instance
      .get("https://jsonplaceholder.typicode.com/users")
      .then(function (response) {
        console.log("Post ", response);
      })
      .catch(function (error) {
        console.log("Error", error);
      });
  };
  //  multiple api call
  const getUsers = () => {
    return instance.get("https://jsonplaceholder.typicode.com/users");
  };
  const getComment = () => {
    return instance.get();
  };

  const Api6 = () => {
    Promise.all([getUsers(), getComment()]).then(function (response) {
      const user = response[0];
      const comments = response[1];
      console.log(user);
      console.log(comments);
    });
  };

  return (
    <div>
      <p>
        <button onClick={Api1}>API Call 1</button>
        <button onClick={Api2}>API Call 2</button>
        <button onClick={Api3}>API Call 3</button>
        <button onClick={Api4}>POST API Call 4</button>
        <button onClick={Api5}>Instance API Call 5</button>
        <button onClick={Api6}>Instance API Call 6</button>
      </p>
    </div>
  );
}
