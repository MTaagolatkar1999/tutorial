import axios from "axios";
import { useState } from "react";
export default function Get() {
  const [movieName, setmovieName] = useState("");
  const [movieReview, setmovieReview] = useState("");
  const [response, setResponse] = useState("");
  const ApiCall = () => {
    axios
      .get(
        `http://localhost:3001/api/insert?movieName=${movieName}&movieReview=${movieReview}`
      )
      .then(function (response) {
        console.log(response.data);
        setResponse(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    ApiCall();
  };

  return (
    <div>
      {response}
      <form onSubmit={handleSubmit}>
        <p>
          Movie Name :{" "}
          <input
            type="text"
            onChange={(e) => {
              setmovieName(e.target.value);
            }}
          />
          {movieName}
        </p>
        <p>
          Movie Review :{" "}
          <input
            type="text"
            onChange={(e) => {
              setmovieReview(e.target.value);
            }}
          />
          {movieReview}
        </p>
        <input type="submit" value="Add Review" className="btn btn-success" />
      </form>
    </div>
  );
}
